from django.urls import path, include
from . import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'articles', views.ArticleViewSet)
urlpatterns = [
    path('', views.index, name='index'),
    path('list/', views.ArticleListView.as_view(), name='articles'),
    path('<int:pk>', views.ArticleDetailView.as_view(), name='article-detail'),
    path('api/', include(router.urls))

    #path('articles/<int:pk>', views.BookDetailView.as_view(), name='article-detail'),
]

