from django.shortcuts import render

from article.models import Article, Category

from django.views import generic

from django.shortcuts import get_object_or_404

from rest_framework import viewsets

from .serializers import ArticleSerializer


class ArticleViewSet(viewsets.ModelViewSet):
    queryset = Article.objects.all().order_by('title')
    serializer_class = ArticleSerializer


def article_detail_view(request, primary_key):
    article = get_object_or_404(Article, pk=primary_key)
    return render(request, 'article_detail.html', context={'article': article})



class ArticleListView(generic.ListView):
    model = Article
    template_name = 'articles.html'


class ArticleDetailView(generic.DetailView):
    model = Article
    template_name = 'article_detail.html'



def index(request):
    """View function for home page of site."""

    num_articles = Article.objects.all().count()
    num_categories = Category.objects.all().count()
    articles = Article.objects.all()

    context = {
        'num_articles': num_articles,
        'num_categories': num_categories,
        'articles_list': articles
    }

    return render(request, 'index.html', context=context)
