from django.contrib import admin
from article.models import Article, Category


class ArticleAdmin(admin.ModelAdmin):
    list_display = ('title', 'category', 'created_at')
    fields = [('title', 'slug', 'cover_image_url'), 'summary', 'content', 'publish_time']
    list_filter = ('created_at', 'category')


admin.site.register(Article, ArticleAdmin)


class CategoryAdmin(admin.ModelAdmin):
    pass


admin.site.register(Category, CategoryAdmin)
