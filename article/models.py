from django.db import models
from django.urls import reverse  # Cette fonction est utilisée pour formater les URL


class Category(models.Model):
    """Cet objet représente une catégorie ou un genre littéraire."""
    name = models.CharField(max_length=200, help_text='Enter an article category (ex : Actualité)')
    image_url = models.CharField(max_length=2048, help_text='Enter an image url for the category', null=True,
                                 blank=True)

    def __str__(self):
        """Cette fonction est obligatoirement requise par Django.
           Elle retourne une chaîne de caractère pour identifier l'instance de la classe d'objet."""
        return self.name


class Article(models.Model):
    title = models.CharField(max_length=200)
    summary = models.TextField(max_length=1000, help_text='Enter a brief description of the article')
    content = models.TextField(help_text='Enter the article content')
    slug = models.CharField(max_length=300,help_text='Enter the article slug')
    cover_image_url = models.CharField(max_length=2048, help_text='Enter an image url for the article')
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)
    publish_time = models.DateTimeField(null=True,help_text='Publication date of the article')

    category = models.ForeignKey(Category, help_text='Select a category for this article', on_delete=models.SET_NULL,
                                 null=True)

    def __str__(self):
        """Fonction requise par Django pour manipuler les objets Book dans la base de données."""
        return self.title

    def get_absolute_url(self):
        """Cette fonction est requise pas Django, lorsque vous souhaitez détailler le contenu d'un objet."""
        return reverse('article-detail', args=[str(self.id)])

